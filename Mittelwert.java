import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      Scanner tastatur = new Scanner(System.in);

      double x;
      double y;
      double m;

      System.out.print('x');
      x = tastatur.nextDouble();
      System.out.print('y');
      y = tastatur.nextDouble();

      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      m = berechnenMittel(x, y);
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }

   public static double berechnenMittel(double x1, double x2) {
      double mittelwert;
      mittelwert = (x1 + x2) / 2.0;

      return mittelwert;

   }
}
