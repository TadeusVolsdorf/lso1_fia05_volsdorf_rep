import java.util.Scanner;

import jdk.javadoc.internal.doclets.toolkit.resources.doclets;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double eingezahlterGesamtbetrag;   // Das ist ein double weil diese variable Kommazahlen enthalten kann. Subtraktion
       double preis;                      // Das ist ein double weil diese variable Kommazahlen enthalten kann. Subtraktion


       preis = fahrkartenauswahl();


       eingezahlterGesamtbetrag = geldeinwurf(preis);

       fahrscheinausgabe();

       System.out.println("\n\n");

       rückgeldberechnung(eingezahlterGesamtbetrag, preis);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }

      // Fahrkartenauswahl, Anzahl sowie der Preis
      // ---------------------------------
      public static double fahrkartenauswahl(){
         Scanner tastatur = new Scanner(System.in);

         int ticketnummer;             // Das ist ein int weil mit der vergleiche gemacht werde.
         double abRegeltarif = 2.90;   // Das ist eine statische Variabel die ein doubel ist weil sie eine Kommazahl ist.
         double abErmäßigt = 1.80;     // Das ist eine statische Variabel die ein doubel ist weil sie eine Kommazahl ist.
         int weiter = 1;               // Das ist ein int, warum weiß ich nicht mehr.
         double zwischenpreis;         // Das ist ein double weil diese variable Kommazahlen enthalten kann. Addition
         double preis = 0;             // Das ist ein double weil diese variable Kommazahlen enthalten kann. Subtraktion
         double ticketwahl = 1;
         int anzahldertickets;

         while(weiter == 1){
            do{
               System.out.printf("Ticketwahl:" +"\n" + "Für Ab-Regeltarif(2,90€) bitte 1 drücken: " + "\n" + "Für Ab-Ermäßigt(1,80€) bitte 2 drücken"+ "\n");
               ticketnummer = tastatur.nextInt();
     
               if(ticketnummer == 1){
                ticketwahl = abRegeltarif;
               }
               if(ticketnummer == 2){
                ticketwahl = abErmäßigt;
               }
               if(ticketnummer > 2) {
                  System.out.println("ERROR WRONG NUMBER");
                  System.out.printf("\n");
               }
            }
            while(ticketnummer > 2);
     
            do{
               System.out.print("Anzahl der Tickets: ");
               anzahldertickets = tastatur.nextInt();

               if(anzahldertickets > 10){
                System.out.printf("ERROR WRONG NUMBER");
                System.out.printf("\n");
               }
            }
            while(anzahldertickets > 10);

            zwischenpreis = ticketwahl * anzahldertickets; // Multiplikation mit einem int und double 
     
            System.out.print("für weitere Tickets bitte die 1 drücken: " + "\n" + "wenn nicht dann bitte die 0 drücken: ");
            weiter = tastatur.nextInt();
            preis = preis + zwischenpreis;
            }

            return preis;
      }

      // Geldeinwurf
      // -----------
      public static double geldeinwurf(double preis){
         Scanner tastatur = new Scanner(System.in);
         
         double eingezahlterGesamtbetrag; // Das ist ein double weil diese variable Kommazahlen enthalten kann.
         double eingeworfeneMünze;        // Das ist ein double weil diese variable Kommazahlen enthalten kann. Addition

         eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < preis)
       {
    	   System.out.printf("%s %.2f %s %n","Noch zu zahlen: ", (preis - eingezahlterGesamtbetrag),"Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
       return eingezahlterGesamtbetrag;
      }
      
      // Fahrscheinausgabe
       // -----------------
      public static void fahrscheinausgabe(){

      System.out.println("\nFahrschein wird ausgegeben");
      for (int i = 0; i < 8; i++)
      {
         System.out.print("=");
         try {
        Thread.sleep(250);
     } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
     }
      }
   }

      // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       public static void rückgeldberechnung(double eingezahlterGesamtbetrag, double preis){
       double rückgabebetrag; // Das ist ein double weil diese variable Kommazahlen enthalten kann.
       double rückgabe;       // Das ist ein double weil diese variable Kommazahlen enthalten kann.

       rückgabebetrag = eingezahlterGesamtbetrag - preis;
       rückgabe = Math.round(rückgabebetrag *100);

       if(rückgabe >= 0.0)
       {
    	   System.out.printf("%s %.2f %s %n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabe >= 200) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabe -= 200;
           }
           while(rückgabe >= 100) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabe -= 100;
           }
           while(rückgabe >= 50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabe -= 50;
           }
           while(rückgabe >= 20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
              rückgabe -= 20;
           }
           while(rückgabe >= 10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabe -= 10;
           }
           while(rückgabe >= 5)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
              rückgabe -= 5;
           }
           while(rückgabe >= 2)// 2 CENT-Münzen
           {
        	  System.out.println("2 CENT");
              rückgabe -= 2;
           }
           while(rückgabe >= 1)// 1 CENT-Münzen
           {
        	  System.out.println("1 CENT");
              rückgabe -= 1;
           }
         }
      
      }
      
      

       
       

}